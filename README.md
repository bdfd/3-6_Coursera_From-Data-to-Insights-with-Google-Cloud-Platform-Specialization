# 3.6_From-Data-to-Insights-with-Google-Cloud-Platform-Specialization
About this Specialization
14,905 recent views
Want to know how to query and process petabytes of data in seconds? Curious about data analysis that scales automatically as your data grows? Welcome to the Data Insights course!

This four-course accelerated online specialization teaches course participants how to derive insights through data analysis and visualization using the Google Cloud Platform. The courses feature interactive scenarios and hands-on labs where participants explore, mine, load, visualize, and extract insights from diverse Google BigQuery datasets. The courses also cover data loading, querying, schema modeling, optimizing performance, query pricing, and data visualization.

This specialization is intended for the following participants:

● Data Analysts, Business Analysts, Business Intelligence professionals

● Cloud Data Engineers who will be partnering with Data Analysts to build scalable data solutions on Google Cloud Platform

To get the most out of this specialization, we recommend participants have some proficiency with ANSI SQL.

>>> By enrolling in this specialization you agree to the Qwiklabs Terms of Service as set out in the FAQ and located at: https://qwiklabs.com/terms_of_service <<<

1. Exploring and Preparing your Data with BigQuery
2. Create New BigQuery Datasets and Visualizing Insights
3. Achieving Advanced Insights with BigQuery
4. Applying Machine Learning to yOUR Data with GCP
